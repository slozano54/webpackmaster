
![Work in progress](http://www.repostatus.org/badges/latest/wip.svg)


<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">STARTER WEBPACK PROJECT</h3>

  <p align="center">
    Having a starter project with webpack.
    <br />
    <a href="https://framagit.org/slozano54/webpackmaster"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://framagit.org/slozano54/webpackmaster">View Demo</a>
    ·
    <a href="https://framagit.org/slozano54/webpackmaster/issues">Report Bug</a>
    ·
    <a href="https://framagit.org/slozano54/webpackmaster/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

### Built With

* []()
* []()
* []()



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

```sh
# Pour récupérer le script d'install il faut curl, et ensuite il aura besoin de lsb-release, et l'usage du dépôt en https demande apt-transport-https  
sudo apt-get install curl apt-transport-https lsb-release
# Récuperation et lancement de l'installateur 
sudo curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
# Installation
sudo apt-get install -y nodejs
# Vérifier la version de nodejs installée
node --version
# Vérifier la version de npm installée
npm --version
# Installer pnpm pour l'utiliser plutôt que npm
sudo npm install -g pnpm
# mettre à jour pnpm
pnpm -g i pnpm
```

### Installation

Clone the repo
   ```sh
   git clone https://framagit.org/slozano54/webpackmaster
   ```
This steps for reach this state :
```sh
# Initaliser le repertoire 
pnpm init -y
# Installer la dernière version de webpack
pnpm install --save-dev webpack@latest webpack-dev-server@latest
# installer babel dans le projet
pnpm i -D babel-loader @babel/core @babel/preset-env
# Installer html-webpack-plugin
pnpm i -D html-webpack-plugin
# Installer terser-webpack-plugin
pnpm i -D terser-webpack-plugin
# Installer mini-css-extract-plugin
pnpm i -D mini-css-extract-plugin
# Installer copy-webpack-plugin
pnpm i -D copy-webpack-plugin
# Installer webpack-dev-server
pnpm i -D webpack-dev-server
# Installer webpack-cli
pnpm i -D webpack-cli
```
<!-- USAGE EXAMPLES -->
## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://framagit.org/slozano54/webpackmaster)_



<!-- ROADMAP -->
## Roadmap

See the [open issues](https://framagit.org/slozano54/webpackmaster/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Sébastien LOZANO - Write me on gitlab

Project Link: [https://framagit.org/slozano54/webpackmaster](https://framagit.org/slozano54/webpackmaster)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* []()
* []()
* []()





