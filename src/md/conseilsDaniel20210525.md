# Message 1
Bonjour Daniel,
je cherche un starter webpack avec les dépendances qui vont bien pour un nouveau projet pour éviter de partir "from scratch".

Je pensais partir de [cette doc](https://github.com/mathalea/mathalea/blob/master/jsdoc/tutorials/internals.md) de mathalea mais pas sûr que la config convienne forcément ni qu'elle soit tout à fait adaptée.

Ou alors adapter le webpack.config.js actuel mais idem pas sûr que ce soit une bonne méthode ni que ça soit adapter.

Peux-tu me conseiller ?

# Réponse 1
Je pense que le meilleur investissement reste de partir de la doc de webpack, tu peux commencer par webpack tout seul (sans babel, je crois que la minification avec terser en mode production est de base), ajouter babel si tu veux, puis éventuellement ajouter css, html & co (avec à chaque fois un loader ou un plugin).

dans la doc de webpack y'a des exemples

# Réponse 2
```js
    entry: './src/js/index.js',
=>

entry: {
  truc: './src/js/index.js'
}
```


et je pense que pour démarrer tu peux virer tous les plugins css, html, copy, terser, garde le max de truc par défaut et n'ajoute des plugins que si t'en as vraiment besoin

# Réponse 3
Tu peux garder babel-loader (tu auras sûrement besoin de babel si tu veux coder avec du js un peu plus moderne que les navigateurs visés), et json-loader si tu veux importer du json dans tes js. le reste tu peux virer (au moins pour le moment). Pour babel revient à du preset-env, c'est un poil optimal que preset-modules quand tu vises les navigateurs modernes, mais bcp plus documenté (et ça t'évite de préciser la conf terser). Sinon tu gardes preset-modules + conf terser (et ajoute un petit bout de js pour détecter les navigateurs trop vieux et les jeter avec un message).

